package com.tulingxueyuan.order.interceptor.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;

public class CustomFeignInterceptor implements RequestInterceptor {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Override
    public void apply(RequestTemplate requestTemplate) {
        // 业务逻辑
        String access_token = UUID.randomUUID().toString();
        requestTemplate.header("Authorization", access_token);
        requestTemplate.query("id", "111");
        requestTemplate.uri("/9");

        logger.info("feign拦截器！");
    }
}
