package com.tuling.sentinelnew.pojo;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import lombok.Data;

@Data
public class User {
    private final String username;

    /**
     * 注意：
     * 1. 一定要public
     * 2. 返回值一定要和源方法保持一致
     * 3. 可以在参数最后添加BlockException，可以用来区分是什么规则的处理方法。
     * @param id
     * @param ex
     * @return
     */
    public static User blockHandlerForGetUser(String id, BlockException ex) {
        ex.printStackTrace();
        return new User("流控！！");
    }
}
